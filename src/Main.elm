module Main exposing (main)

import Browser
import Browser.Dom exposing (getViewport)
import Browser.Events as DomEvents
import Dict
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import File exposing (File)
import File.Download as Download
import File.Select as Select
import Html exposing (Html)
import Task


main : Program () Model Affect
main =
    Browser.document
        { init = initalValues
        , update = effects
        , view = view
        , subscriptions = subs
        }


view : Model -> Browser.Document Affect
view model =
    Browser.Document (model.title ++ " - Essay Writer") [ layout model ]



{- Main -}


type alias Model =
    { mode : State
    , title : String
    , author : String
    , paragraphs : Paragraphs
    , paragraphNames : Paragraphs
    , paragraphCount : Int
    , paragraphSelected : Dict.Dict Int Bool
    , wordCount : Wordcount
    , device : Device
    }


type State
    = Loading
    | Config
    | Topic
    | Edit


type alias Paragraphs =
    Dict.Dict Int String


type alias Wordcount =
    { current : Int
    , desired : Maybe Int
    }


initalValues : () -> ( Model, Cmd Affect )
initalValues _ =
    ( { mode = Config
      , title = "Untitled"
      , author = "Author Name"
      , paragraphCount = 0
      , paragraphs = Dict.fromList [ ( 1, "" ) ]
      , paragraphNames = Dict.fromList [ ( 1, "Intro" ) ]
      , paragraphSelected = Dict.empty
      , wordCount = Wordcount 0 Nothing
      , device = Element.Device Desktop Landscape
      }
    , Cmd.none
    )


demoInitalValues : () -> ( Model, Cmd Affect )
demoInitalValues _ =
    ( { mode = Loading
      , title = "Varieties of Cheese"
      , author = "Not Cow"
      , paragraphCount = 5
      , paragraphs =
            Dict.fromList <|
                (\xs content -> List.map2 Tuple.pair xs content)
                    (List.range 1 5)
                    (List.repeat 5 "")
      , paragraphNames =
            Dict.fromList <|
                (\xs name -> List.map2 Tuple.pair xs name)
                    (List.range 1 5)
                    [ "Intro", "Cheese", "Milk", "Utter", "Conclusion" ]
      , paragraphSelected =
            Dict.fromList <|
                (\xs content -> List.map2 Tuple.pair xs content)
                    (List.range 1 5)
                    (List.repeat 5 False)
      , wordCount = Wordcount 0 (Just 300)
      , device = Element.Device Desktop Landscape
      }
    , Task.perform GotViewport getViewport
    )



{- Update -}


type
    Affect
    -- Usually, something "affects" something to produce an "effect."
    = StartTopic
    | StartEdit
    | Title String
    | Author String
    | ParagraphCount Int
    | ParagraphName Int String
    | ParagraphContent Int String
    | ParagraphSelected Int
    | CountWord { current : Int, desired : Maybe Int }
    | GotViewport Browser.Dom.Viewport
    | Device Element.Device
    | UploadEssay
    | SelectEssay File
    | ImportEssay String
    | DownloadEssay


effects : Affect -> Model -> ( Model, Cmd Affect )
effects affects model =
    case affects of
        StartTopic ->
            ( { model
                | mode = Topic
                , paragraphNames =
                    Dict.insert
                        model.paragraphCount
                        "Conclusion"
                        model.paragraphNames
              }
            , Cmd.none
            )

        StartEdit ->
            ( { model | mode = Edit }, Cmd.none )

        Title input ->
            ( { model | title = input }, Cmd.none )

        Author input ->
            ( { model | author = input }, Cmd.none )

        ParagraphCount input ->
            ( { model
                | paragraphCount = input
                , paragraphs =
                    Dict.fromList <|
                        (\xs content -> List.map2 Tuple.pair xs content)
                            (List.range 1 input)
                            (List.repeat (input - 1) "" ++ [ "Conclusion" ])
                , paragraphSelected =
                    Dict.fromList <|
                        (\xs bool -> List.map2 Tuple.pair xs bool)
                            (List.range 1 input)
                            (List.repeat input False)
              }
            , Cmd.none
            )

        ParagraphName number name ->
            ( { model
                | paragraphNames = Dict.insert number name model.paragraphNames
              }
            , Cmd.none
            )

        ParagraphContent number content ->
            ( { model
                | paragraphs = Dict.insert number content model.paragraphs
              }
            , Cmd.none
            )

        ParagraphSelected number ->
            ( { model
                | paragraphSelected =
                    Dict.map
                        (\key _ -> key == number)
                        model.paragraphSelected
              }
            , Cmd.none
            )

        CountWord { desired } ->
            case desired of
                Just _ ->
                    ( { model
                        | wordCount = Wordcount model.wordCount.current desired
                      }
                    , Cmd.none
                    )

                Nothing ->
                    ( { model
                        | wordCount =
                            Wordcount model.wordCount.current (Just 0)
                      }
                    , Cmd.none
                    )

        GotViewport viewport ->
            ( { model
                | mode = Edit
                , device =
                    classifyDevice
                        { height = round viewport.viewport.height
                        , width = round viewport.viewport.width
                        }
              }
            , Cmd.none
            )

        Device deviceType ->
            ( { model | device = deviceType }, Cmd.none )

        UploadEssay ->
            ( model, Select.file [ "text/markdown" ] SelectEssay )

        SelectEssay essay ->
            ( model
            , Task.perform ImportEssay (File.toString essay)
            )

        ImportEssay essay ->
            let
                essayRecord =
                    fileToEssay essay
            in
            ( { model
                | mode = Edit
                , wordCount = Wordcount model.wordCount.current (Just essayRecord.wordcount)
                , paragraphs = essayRecord.content
                , paragraphNames = essayRecord.titles
                , paragraphCount = Dict.size essayRecord.titles
                , paragraphSelected =
                    Dict.fromList <|
                        List.indexedMap (\int _ -> Tuple.pair (int + 1) False) <|
                            Dict.toList essayRecord.titles
              }
            , Cmd.none
            )

        DownloadEssay ->
            ( model
            , Download.string (model.title ++ " essay.md")
                "text/markdown"
                (essayToFile ( model.title, model.author )
                    model.paragraphNames
                    model.paragraphs
                    (Maybe.withDefault 0 model.wordCount.desired)
                )
            )


fileToEssay : String -> { titles : Paragraphs, content : Paragraphs, wordcount : Int }
fileToEssay essay =
    -- First paragraph is the titles, second is the paragraph content
    let
        paragraphTitles =
            Dict.fromList <|
                List.indexedMap (\int string -> Tuple.pair (int + 1) string) <|
                    List.map (\string -> String.dropLeft 2 string) <|
                        List.filter (String.startsWith "# ") <|
                            String.split "\n" essay

        paragraphs =
            Dict.fromList <|
                List.indexedMap (\int string -> Tuple.pair (int + 1) string) <|
                    List.filter
                        (\string ->
                            not (String.startsWith "# " string || string == "")
                        )
                    <|
                        List.drop 3 (String.split "\n" essay)

        wordcount =
            Maybe.withDefault 0 <|
                String.toInt <|
                    String.dropLeft 16 <|
                        String.dropRight 1 <|
                            Maybe.withDefault "" <|
                                List.head <|
                                    List.filter (String.startsWith "[wordcount]") <|
                                        String.split "\n" essay
    in
    { titles = paragraphTitles
    , content = paragraphs
    , wordcount = wordcount
    }


essayToFile : ( String, String ) -> Paragraphs -> Paragraphs -> Int -> String
essayToFile ( title, author ) paragraphTitles paragraphs wordCount =
    let
        oneLine =
            \starts content -> starts ++ content ++ "\n"

        essay =
            List.map2 Tuple.pair (Dict.values paragraphTitles) (Dict.values paragraphs)

        metadata =
            "[wordcount]: # (" ++ String.fromInt wordCount ++ ")\n"

        removeNewline paragraph =
            if String.endsWith "\n" paragraph then
                String.dropRight 2 paragraph

            else
                paragraph
    in
    List.foldl
        (\content output ->
            output
                ++ "\n"
                ++ oneLine "# " (Tuple.first content)
                ++ oneLine "" (removeNewline (Tuple.second content))
        )
        (oneLine "% " title ++ oneLine "% " author ++ metadata)
        essay



{- Subscriptions -}


subs : Model -> Sub Affect
subs _ =
    DomEvents.onResize
        (\width height ->
            Device (classifyDevice { height = height, width = width })
        )



{- Layout -}


layout : Model -> Html Affect
layout model =
    Element.layoutWith
        { options = [ focusStyle (FocusStyle Nothing Nothing Nothing) ] }
        [ statBar model
        , Background.color neumorph.element
        ]
        (document model)


document : Model -> Element Affect
document model =
    case model.mode of
        Config ->
            configUi model

        Topic ->
            topicUi model

        Edit ->
            editorUi model

        Loading ->
            el
                [ centerX, centerY, Font.size 24, Font.color (hex 0x006D4216 0.85) ]
                (text "Essay Writer")



{- Configuation Ui -}


configUi : Model -> Element Affect
configUi model =
    let
        framing content =
            el [ width fill, centerX, centerY ] (contentPadding 10 content)

        menu =
            column [ centerX, centerY, spacing 15, width (fillPortion 1) ]
                [ text "Configure your document"
                , configInput Title model.title "Document Title:"
                , configInput Author model.author "Document Author:"
                , configInput
                    (\int -> CountWord { current = 0, desired = String.toInt int })
                    (String.fromInt <| Maybe.withDefault 0 model.wordCount.desired)
                    "Word Count:"
                , configDropBox ParagraphCount
                    model.paragraphCount
                    9
                    "Paragraph Count:"
                , Input.button [ centerX ]
                    { onPress = Just StartTopic, label = text "done!" }
                ]

        subMenu =
            selectSave

        selectSave =
            column [ width (fillPortion 1), height fill ]
                [ Input.button [ centerX, centerY ]
                    { label =
                        el (padding 12 :: popOut (roundBy 30) 5)
                            (text "Click here to select a previous save here to resume editing!")
                    , onPress = Just UploadEssay
                    }
                ]
    in
    framing <|
        row
            [ centerX, centerY, spacing 15, width fill ]
            [ menu
            , subMenu
            ]


configInput : (String -> msg) -> String -> String -> Element msg
configInput affect inputString annotation =
    row
        ([ spacing 5, width fill, centerY ]
            ++ padding 10
            :: popOut (roundBy 30) 2
        )
        [ el [ alignLeft, width fill ] (text annotation)
        , Input.text
            ([ alignRight
             , width fill
             , Border.width 0
             , Border.roundEach
                { topRight = 30, bottomRight = 30, topLeft = 0, bottomLeft = 0 }
             ]
                ++ popOut (roundBy 30) 3
            )
            { onChange = affect
            , label = Input.labelHidden "fuck"
            , text = inputString
            , placeholder = Nothing
            }
        ]


configDropBox : (Int -> msg) -> Int -> Int -> String -> Element msg
configDropBox affect value maxValue annotation =
    -- Takes a list of values then creates a button
    -- for each value inacting an related affect
    row [ spacing 5, width fill, padding 10 ]
        [ el [ alignLeft, width fill ] (text annotation)
        , row [ centerX, width fill, spaceEvenly ] <|
            List.map
                (\number ->
                    el [ width fill ] (configButton affect value number)
                )
                (List.range 5 maxValue)
        ]


configButton : (Int -> msg) -> Int -> Int -> Element msg
configButton affect value int =
    Input.button
        ([ centerX, centerY, padding 2 ]
            ++ (if int == value then
                    popIn (roundBy 20) 1

                else
                    popOut (roundBy 20) 1
               )
        )
        { onPress = Just (affect int)
        , label =
            el
                [ centerX, height (px 30), width (px 30) ]
            <|
                el [ centerX, centerY ] (text <| String.fromInt int)
        }



{- Topic input Ui -}


topicUi : Model -> Element Affect
topicUi model =
    column [ centerX, centerY, paddingXY 0 15, spacing 30 ]
        [ column [ centerX, centerY, spacing 15 ] <|
            List.concat
                (List.map (topicConstructor model)
                    (List.range 1 model.paragraphCount)
                )
        , Input.button
            ([ centerX, padding 10 ]
                ++ popOut (roundBy 20) 3
            )
            { onPress = Just StartEdit, label = text "Done!" }
        ]


topicConstructor : Model -> Int -> List (Element Affect)
topicConstructor model para =
    [ topicInput (ParagraphName para)
        (Maybe.withDefault "" (Dict.get para model.paragraphNames))
        para
    ]


topicInput : (String -> msg) -> String -> Int -> Element msg
topicInput affect inputString num =
    row
        ([ centerX, width fill ]
            ++ padding 10
            :: popOut (roundBy 20) 2
        )
        [ el [ width fill ] (text ("Topic for paragraph " ++ String.fromInt num))
        , Input.text
            ([ width fill, padding 10 ]
                ++ popOut (roundBy 30) 3
            )
            { onChange = affect
            , label = Input.labelHidden "fuck"
            , text = inputString
            , placeholder = Nothing
            }
        ]



{- Editor Ui -}


editorUi : Model -> Element Affect
editorUi model =
    let
        conditionalPadding =
            if model.device.class == Desktop then
                [ width (fillPortion 1) ]

            else
                [ above none ]

        conditionalPop =
            if model.device.class == Desktop then
                padding 60
                    :: popOut (roundBy 20) 6

            else
                [ paddingXY 20 0 ]
    in
    column
        [ width fill
        , padding 5
        ]
        [ row [ width fill, paddingXY 0 10 ]
            [ column (padding 10 :: popOut (roundBy 10) 3)
                [ el [] (text ("Title: " ++ model.title))
                , el [] (text ("Author: " ++ model.author))
                ]
            , column (alignRight :: padding 10 :: popOut (roundBy 10) 3)
                [ el [ alignRight ] (text "Stats")
                , Input.button []
                    { label = text "Download Content"
                    , onPress = Just DownloadEssay
                    }
                ]
            ]
        , row [ width fill, paddingXY 0 20 ]
            [ el conditionalPadding none
            , column
                ([ centerX, centerY, spacing 5, width (fillPortion 7) ]
                    ++ conditionalPop
                )
              <|
                List.concat
                    (List.map (editorConstructor model)
                        (Dict.keys model.paragraphs)
                    )
            , el conditionalPadding none
            ]
        ]


editorConstructor : Model -> Int -> List (Element Affect)
editorConstructor model para =
    -- Pieces together the editor layout
    let
        fontAdjustment =
            case model.device.class of
                BigDesktop ->
                    [ spacing 1
                    , Font.size 12
                    , lineInput 24 12
                    ]

                Desktop ->
                    [ spacing 1
                    , Font.size 12
                    , lineInput 24 12
                    ]

                Phone ->
                    [ spacing 3
                    , Font.size 20
                    , lineInput 32 22
                    ]

                Tablet ->
                    [ spacing 3
                    , Font.size 20
                    , lineInput 32 22
                    ]
    in
    if showParagraphs para model.paragraphSelected then
        [ el
            ([ width fill, height fill ]
                ++ borderHighlighter
                    (Maybe.withDefault False <|
                        Dict.get para model.paragraphSelected
                    )
            )
            (paragraphInput
                ([ Events.onFocus (ParagraphSelected para)
                 , Border.color (rgba 0 0 0 0)
                 , Background.color (rgba 0 0 0 0)
                 ]
                    ++ fontAdjustment
                )
                (ParagraphContent para)
                (Maybe.withDefault "" (Dict.get para model.paragraphs))
                (Maybe.withDefault "" (Dict.get para model.paragraphNames))
            )
        , row
            [ width fill
            , paddingEach { each | top = 5, bottom = 10 }
            , Font.size 14
            ]
            [ el [ alignLeft ] <| text (support para model)
            , el [ alignRight ] <|
                text <|
                    String.fromInt <|
                        wordFinder <|
                            [ Maybe.withDefault "" <|
                                Dict.get para model.paragraphs
                            ]
            ]
        ]

    else
        [ Input.button [ centerX ]
            { label = text "..."
            , onPress = Just (ParagraphSelected para)
            }
        ]


paragraphInput : List (Attribute msg) -> (String -> msg) -> String -> String -> Element msg
paragraphInput attributes affect inputString name =
    row [ width fill, spacing 5 ]
        [ column [ alignTop ]
            (List.map (\input -> el [] (text (String.fromChar input))) <|
                String.toList name
            )
        , Input.multiline
            ([ height (px 150)
             , height fill
             , clipY
             ]
                ++ attributes
            )
            { onChange = affect
            , text = inputString
            , placeholder = Nothing
            , label = Input.labelHidden "frick"
            , spellcheck = True
            }
        ]


lineInput : Int -> Int -> Attribute msg
lineInput topPadding lineHeight =
    behindContent
        (row [ width fill, height fill, clipY, spacing 10 ]
            [ el [] none
            , column
                [ width <| fillPortion 60
                , height fill
                , spacing lineHeight
                , paddingEach { each | top = topPadding }
                ]
                (List.repeat 40
                    (el
                        [ width fill
                        , Background.color neumorph.dark
                        , moveDown 1
                        , height (px 1)
                        ]
                        none
                    )
                )
            , el [ width <| fillPortion 1 ] none
            ]
        )



{- Componants -}


contentPadding : Int -> Element msg -> Element msg
contentPadding padding content =
    row [ width fill ]
        [ el [ width (fillPortion 1) ] none
        , el [ width (fillPortion padding) ] content
        , el [ width (fillPortion 1) ] none
        ]


statBar : Model -> Attribute msg
statBar model =
    if model.mode == Edit then
        inFront
            (row
                [ alignBottom
                , width fill
                , paddingEach { each | top = 1, bottom = 3 }
                , Background.color (rgb 1 1 1)
                , Border.widthEach { each | top = 1 }
                , Border.solid
                ]
                [ sentenceCounter [ alignLeft ] <|
                    sentenceFinder <|
                        String.join " " (Dict.values model.paragraphs)
                , wordCounter [ alignRight ] model
                ]
            )

    else
        above none


borderHighlighter : Bool -> List (Attribute msg)
borderHighlighter active =
    if active then
        padding 10 :: popOut (roundBy 9) 3

    else
        padding 10 :: popIn (roundBy 9) 3


popOut : Rounding -> Float -> List (Attribute msg)
popOut round size =
    [ Border.roundEach round
    , Border.shadow
        { offset = ( size, size )
        , size = size
        , blur = size * 4
        , color = neumorph.dark
        }
    , behindContent
        (el
            [ width fill
            , height fill
            , Border.roundEach round
            , Border.shadow
                { offset = ( negate size, negate size )
                , size = size
                , blur = size * 4
                , color = neumorph.light
                }
            ]
            none
        )
    ]


popIn : Rounding -> Float -> List (Attribute msg)
popIn round size =
    [ Border.roundEach round
    , Border.innerShadow
        { offset = ( size, size )
        , size = size
        , blur = size * 4
        , color = neumorph.dark
        }
    , behindContent
        (el
            [ width fill
            , height fill
            , Border.roundEach round
            , Border.innerShadow
                { offset = ( negate size, negate size )
                , size = size
                , blur = size * 4
                , color = neumorph.light
                }
            ]
            none
        )
    ]


showParagraphs : Int -> Dict.Dict Int Bool -> Bool
showParagraphs para dict =
    -- Determines if a given paragraph is next to the selected or above it
    let
        selected =
            Maybe.withDefault 0 <|
                List.head (Dict.keys (Dict.filter (\_ value -> value) dict))
    in
    para <= selected || (para - 1) == selected



{- prose helpers -}


support : Int -> Model -> String
support para model =
    -- decides what kind of support is required and provides it
    let
        selected =
            Maybe.withDefault False (Dict.get para model.paragraphSelected)
    in
    if para == 1 && selected then
        introSupporter para model.paragraphs

    else if para == Dict.size model.paragraphs && selected then
        conclSupporter para model.paragraphs

    else if selected then
        bodySupporter para model.paragraphs model.paragraphNames

    else
        ""


introSupporter : Int -> Paragraphs -> String
introSupporter para dict =
    -- introduction : Thesis -> List (Themes) -> signposts
    -- introduction mainAgument themes thematicframework =
    let
        sentenceNum =
            1 + (sentenceFinder <| Maybe.withDefault "" <| Dict.get para dict)
    in
    if sentenceNum == 1 then
        "Your thesis, your main argument or idea, right here."

    else if sentenceNum == 2 || sentenceNum == 3 then
        "List and explain the topics you'll be covering."

    else if sentenceNum == 4 then
        "Explain how these ideas logically fit together"

    else
        "Whoa buster, slow down end this paragraph"


bodySupporter : Int -> Paragraphs -> Paragraphs -> String
bodySupporter para dict topic =
    -- Gives advice according to the current number sentince in a given
    -- body paragraph.
    let
        sentenceNum =
            1 + (sentenceFinder <| Maybe.withDefault "" <| Dict.get para dict)

        currentTopic =
            Maybe.withDefault "" <| Dict.get para topic
    in
    if sentenceNum == 1 then
        "Try state your topic, " ++ currentTopic ++ ", in the first sentence."

    else if sentenceNum == 2 then
        "State some facts about your topic in this sentence"

    else if sentenceNum == 3 then
        "Elaborate on what those facts mean"

    else if sentenceNum == 4 then
        "Wrap up this paragraph or move on your next piece of your topic"

    else if sentenceNum == 5 then
        "State more facts about this paragraphs topic"

    else if sentenceNum == 6 then
        "Elaborate on how those facts affect your topic"

    else if sentenceNum == 7 then
        "Link those atopics together and wrap up this paragraph"

    else if sentenceNum == 8 then
        "Sentence if beginning to run on, I would recommend cutting it off"

    else
        "Whoa buster, slow down and start a new paragraph."


conclSupporter : Int -> Paragraphs -> String
conclSupporter para dict =
    -- conclusion Thesis -> List Themes -> closing statement
    -- conclusion mainAgument themes closing =
    let
        sentenceNum =
            1 + (sentenceFinder <| Maybe.withDefault "" <| Dict.get para dict)
    in
    if sentenceNum == 1 then
        "Restate your thesis"

    else if sentenceNum == 2 || sentenceNum == 3 || sentenceNum == 4 then
        "Recap your key ideas"

    else if sentenceNum == 5 then
        "End with a closing statement"

    else
        "This is beginning to run on"



{- Calculators -}


wordCounter : List (Attribute msg) -> Model -> Element msg
wordCounter attributes model =
    el attributes
        (text
            ("Word-count: "
                ++ (String.fromInt <| wordFinder <| Dict.values model.paragraphs)
                ++ "/"
                ++ (String.fromInt <|
                        Maybe.withDefault 0 model.wordCount.desired
                   )
            )
        )


wordFinder : List String -> Int
wordFinder words =
    List.length <|
        String.words <|
            String.map
                (\char ->
                    if char == '"' then
                        ' '

                    else
                        char
                )
            <|
                String.join
                    " "
                    words


sentenceCounter : List (Attribute msg) -> Int -> Element msg
sentenceCounter attributes int =
    el attributes (text <| "Sentences: " ++ String.fromInt int)


sentenceFinder : String -> Int
sentenceFinder sentences =
    List.length <|
        List.filter (\word -> String.endsWith "." word) <|
            String.words <|
                String.map
                    (\char ->
                        if char == '?' || char == '!' || char == '…' then
                            '.'

                        else
                            char
                    )
                    sentences



{- Design Constants -}


each : { top : Int, bottom : Int, left : Int, right : Int }
each =
    { top = 0, bottom = 0, left = 0, right = 0 }


type alias Rounding =
    { topLeft : Int
    , topRight : Int
    , bottomLeft : Int
    , bottomRight : Int
    }


roundBy : Int -> Rounding
roundBy num =
    -- Record for consistant Border.roundEach creation
    { topLeft = num
    , topRight = num
    , bottomLeft = num
    , bottomRight = num
    }


hex : Int -> Float -> Color
hex anyNumber alpha =
    let
        number =
            clamp 0 0x00FFFFFF anyNumber

        b =
            modBy 0x0100 number

        rg =
            number // 0x0100

        g =
            modBy 0x0100 rg

        r =
            rg // 0x0100
    in
    rgba255 r g b alpha


neumorph :
    { dark : Color
    , light : Color
    , element : Color
    }
neumorph =
    { dark = hex 0x006D4216 0.15
    , light = hex 0x00FEFCFA 0.6
    , element = hex 0x00F5DFC9 1
    }
