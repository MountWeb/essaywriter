#+TITLE: Essay Writer Read-me

* Purpose
A tool for writing an essay stepping through one step at a time using the Point, Evidence, Explanation, Link/Point, Evidence, Justification, Corroboration system.

** To whom
For goal seekers and people who want to see their progress as they type their essay.

** How will this work?
This will run through several modes, one where the user sets up the initial configuration of the document, then dives into the topics and then creates a editing environment suitable.

* Timeline [63%]
- [X] Document Metadata (Title, Author, word count/paragraphs)
- [X] Editing layout
- [X] Overall document stats
- [X] Per-paragraph stats
- [X] Topic layout
- [X] Paragraph based guide
- [ ] "Achievements", milestone motivators or variable schedule reinforcement
- [ ] Pretty Refactor
- [ ] elm-markup back-end.
- [ ] Polishment
- [X] File Export

* Features
- At various points in a users writing give suggestions (e.g. as the user is writing their first sentence, give a recommendation above the paragraph to state their topic.) These recommendations occur in relation to what a user sets up as their topic in the topic stage

* Design Goals
Point system, gems based on achievements. (Inkscape.)

- Tier 1 - Triangle green gem
- Tier 2 - Diamond red gem
- Tier 3 - Pentagon blue gem
- Tier 4 - Round sapphire
- Tier 5 - Gold five-point diamond
- Tier 6/special - Purple gem shards
